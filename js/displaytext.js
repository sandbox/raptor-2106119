//wait till document is ready
jQuery( 'document').ready(function() {
	
	// set timer to poll console log
	setInterval(function() {
		
		//poll console log via ajax
		jQuery.ajax({
			
			url: Drupal.settings.basePath + '?q=js-callback',
			dataType: 'json',
			success: function( data ) {
				//set text
				jQuery( '[name=mcconsoleform]' ).text(data.msg);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.status);
				console.log(thrownError);
			}
			
  		});
  		
	}, 2000); //1 second = 1000
});

